import BookList from "components/BookList";
import React from "react";
import "app.css";

const App = () => {
    return (
        <React.Fragment>
            <BookList />
        </React.Fragment>
    );
};

export default App;

const Events = () => {
    const handleButtonClick = () => {
        alert("handle button click!");
    };

    const handleFormInput = (e) => {
        // console.log(e);
        console.log(e.target);
        console.log(e.target.name);
        console.log(e.target.value);
        // console.log("handle form input");
    };

    const handleSubmitForm = (e) => {
        e.preventDefault();
        console.log("form submitted");
    };
    return (
        <section>
            <form>
                <h2>Typical Form</h2>
                <input type="text" name="example" style={{ margin: "1rem 0" }} onChange={handleFormInput} />
                <button type="submit" onClick={handleSubmitForm}>
                    Submit
                </button>
                <div>
                    <button onClick={handleButtonClick} type="button">
                        Click Me
                    </button>
                </div>
            </form>
        </section>
    );
};
export default Events;

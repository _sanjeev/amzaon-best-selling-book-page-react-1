const Book = (props) => {
    return (
        <div className="book">
            <img src={props.image} alt={props.title} />
            <h2>{props.title}</h2>
            <h4>{props.author.toUpperCase()}</h4>
            <span className="number"># {props.id}</span>
        </div>
    );
};

export default Book;

import React from "react";
import { allBooks } from "utils/constant";
import Book from "./Book";

const BookList = () => {
    return (
        <>
            <h1>amazon best selling books</h1>
            <div className="booklist">
                {allBooks.map((book) => (
                    <Book {...book} key={book.id} />
                ))}
            </div>
        </>
    );
};

export default BookList;

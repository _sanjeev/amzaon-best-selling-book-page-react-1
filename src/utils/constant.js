import img2 from "../images/img2.jpg";
import img3 from "../images/img3.jpg";
import img4 from "../images/img4.jpg";
export const allBooks = [
    {
        id: 1,
        title: "KVS Pedagogy Master Book",
        author: "Rohit Vaidwan",
        image: img2,
    },
    {
        id: 2,
        title: "Energize Your Mind",
        author: "Gaur Gopal Das",
        image: img2,
    },
    {
        id: 3,
        title: "Atomic Habits",
        author: "James Clear",
        image: img3,
    },
    {
        id: 4,
        title: "The Psychology of Money",
        author: "Morgan Housel",
        image: img4,
    },
];
